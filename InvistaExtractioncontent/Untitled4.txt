 Report For DocBase DemoRepo As Of 9/8/2019 19:17:40
   



######################################################################
##                                                                  ##
##                   dm_DataDictionaryPublisher                     ##
##                   --------------------------                     ##
##                                                                  ##
## The dm_DataDictionaryPublisher publishes all Data Dictionary     ##
## information.                                                     ##
## The dm_DataDictionaryPublisher publishes information for all     ##
## types and attributes in each of the dd_locales specified in the  ##
## dm_docbase_config object for the docbase.                        ##
##                                                                  ##
##     Start Time: 09-08-2019 19:17:41                              ##
##                                                                  ##
######################################################################


 utility syntax: apply,c,NULL,RESYNC_DATA_DICTIONARY,RETRY_COUNT,i,10
Executing ...
The  job was successful.
 
 
Publish Job Statistics: 
--------------------------------------------------------- 
 
  publish_count         : 0
  failure_count         : 0
  retry_count           : 10
  publish_start_time    : 9/8/2019 7:17:41 PM
  publish_complete_time : 9/8/2019 7:17:42 PM
 
 
Report End  9/8/2019 19:17:43
